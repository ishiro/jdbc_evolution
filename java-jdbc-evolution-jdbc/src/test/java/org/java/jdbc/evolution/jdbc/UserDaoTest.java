package org.java.jdbc.evolution.jdbc;

import java.util.Date;

import org.java.jdbc.evolution.jdbc.dao.UserDaoImp;
import org.java.jdbc.evolution.jdbc.po.User;

import junit.framework.TestCase;

public class UserDaoTest extends TestCase{

	public void testUserInsert(){
		UserDaoImp daoImp = new UserDaoImp();
		User u = new User();
		u.setNickName("李白");
		u.setAccount("libai");
		u.setPassword("libai");
		u.setUpdateTime(new Date());
		u.setCreateTime(new Date());
		daoImp.insertUser(u);
	}
	
	public void testUserUpdate(){
		UserDaoImp daoImp = new UserDaoImp();
		User u = new User();
		u.setId(1);
		u.setNickName("李白");
		u.setAccount("libai");
		u.setPassword("libai");
		u.setUpdateTime(new Date());
		u.setCreateTime(new Date());
		daoImp.updateUser(u);
	}
	
	public void testUserSelectById(){
		UserDaoImp daoImp = new UserDaoImp();
		System.out.println(daoImp.selectUserById(1));
	}
	
	public void testUserSelectList(){
		UserDaoImp daoImp = new UserDaoImp();
		System.out.println(daoImp.selectUserList());
	}
}
