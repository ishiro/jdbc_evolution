package org.java.jdbc.evolution.hibernate.dao;

import java.util.List;
import org.java.jdbc.evolution.hibernate.po.User;


public interface UserDao {
	public int insertUser(User user);
	
	public int updateUser(User user);
	
	public User selectUserById(int id);
	
	public List<User> selectUserList();
}
