package org.java.jdbc.evolution.hibernate.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.java.jdbc.evolution.hibernate.po.User;


/**
 */
public class UserDaoImp implements UserDao {

	private SessionFactory sessionFactory;
	
	public UserDaoImp(SessionFactory sessionFactory){
		this.sessionFactory = sessionFactory;
	}
	
	public Session getSession(){
		return sessionFactory.openSession();
	}
	
	public int insertUser(User user) {
		Session session = getSession();
		Transaction t = session.beginTransaction();
		t.begin();
		session.save(user);
		t.commit();
		return 0;
	}

	public int updateUser(User user) {
		sessionFactory.openSession().saveOrUpdate(user);
		return 0;
	}

	public User selectUserById(int id) {
		return sessionFactory.openSession().get(User.class, id);
	}

	public List<User> selectUserList() {
		return sessionFactory.openSession().createQuery("select * from user").list();
	}

}
