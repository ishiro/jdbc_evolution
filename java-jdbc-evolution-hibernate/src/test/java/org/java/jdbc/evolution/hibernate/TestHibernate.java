package org.java.jdbc.evolution.hibernate;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.java.jdbc.evolution.hibernate.dao.UserDao;
import org.java.jdbc.evolution.hibernate.dao.UserDaoImp;
import org.java.jdbc.evolution.hibernate.po.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-jdbc-hibernate.xml")
public class TestHibernate {

	@Autowired
	private UserDaoImp userDao;
	
	@Test
	public void testSpringHibernate(){
		User u = new User();
		u.setNickName("hibernate");
		u.setAccount("hbn");
		u.setPassword("hbm123");
		u.setUpdateTime(new Date());
		u.setCreateTime(new Date());
		userDao.insertUser(u);
	}
	
	@Test
	public void testInsert(){
		System.out.println("begin");
		//D:\workerspaces\wp1\java-jdbc-evolution-hibernate\target\classes\hibernate-config
		System.out.println(TestHibernate.class.getResource("/"));
		InputStream in = TestHibernate.class.getResourceAsStream("/hibernate.cfg.xml");
		if(in == null){
			System.out.println("not find file");
			return;
		}
		byte[] bytes = new byte[1024];
		try {
			while(in.read(bytes) != -1){
				System.out.println(new String(bytes));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("end");
		Configuration cfg = new Configuration();
		cfg.configure("/hibernate.cfg.xml");
		
		SessionFactory factory = cfg.buildSessionFactory();
		Session session = factory.openSession();
		
		Transaction t = session.beginTransaction();
		
		User u = new User();
		u.setNickName("hibernate");
		u.setAccount("hbn");
		u.setPassword("hbm123");
		u.setUpdateTime(new Date());
		u.setCreateTime(new Date());
		
		session.persist(u);
		
		t.commit();
		session.close();
		
	}
}
