package org.java.jdbc.evolution.mybatis.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.java.jdbc.evolution.mybatis.po.User;

/**
 * 用户表 db接口
 */
public interface UserDao {
	
	public int insertUser(User user);
	
	public int updateUser(User user);
	
	public User selectUserById(int id);
	
	public List<User> selectUserList();
	
	@Select("SELECT * FROM user WHERE id = #{id}")
	public User getUser(int id);
	
}
