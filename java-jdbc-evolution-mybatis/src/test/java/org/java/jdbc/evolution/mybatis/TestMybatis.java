package org.java.jdbc.evolution.mybatis;

import org.java.jdbc.evolution.mybatis.dao.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:mybatis-spring.xml")
public class TestMybatis {
	
	@Autowired
	private UserDao userDao;
	
	@Test
	public void getUser(){
		System.out.println(userDao.getUser(1));
	}
}
