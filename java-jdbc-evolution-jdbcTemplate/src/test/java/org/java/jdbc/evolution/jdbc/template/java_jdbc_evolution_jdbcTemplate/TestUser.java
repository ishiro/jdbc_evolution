package org.java.jdbc.evolution.jdbc.template.java_jdbc_evolution_jdbcTemplate;

import java.util.Date;

import org.java.jdbc.evolution.jdbc.template.dao.UserDaoImp;
import org.java.jdbc.evolution.jdbc.template.po.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-jdbc.xml")
public class TestUser {
	
	@Autowired
	private UserDaoImp daoImp;
	
	@Test
	public void test1(){
		System.out.println(111);
	}
	
	@Test
	public void testUserInsert(){
		User u = new User();
		u.setNickName("茅屋被秋风所破歌");
		u.setAccount("dufu");
		u.setPassword("dufu");
		u.setUpdateTime(new Date());
		u.setCreateTime(new Date());
		daoImp.insertUser(u);
	}
	
	public void testUserUpdate(){
		User u = new User();
		u.setId(4);
		u.setNickName("茅屋被秋风所破歌");
		u.setAccount("dufu");
		u.setPassword("dufu");
		u.setUpdateTime(new Date());
		u.setCreateTime(new Date());
		daoImp.updateUser(u);
	}
	@Test
	public void testUserSelectById(){
		System.out.println(daoImp.selectUserById(1));
	}
	@Test
	public void testUserSelectList(){
		System.out.println(daoImp.selectUserList());
	}
}
