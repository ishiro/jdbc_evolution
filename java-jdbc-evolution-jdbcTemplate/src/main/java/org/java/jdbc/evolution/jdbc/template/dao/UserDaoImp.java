package org.java.jdbc.evolution.jdbc.template.dao;

import java.util.List;
import org.java.jdbc.evolution.jdbc.template.po.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 * 用户表接口实现
 */
public class UserDaoImp implements UserDao {

	private JdbcTemplate jdbcTemplate;
	
	public UserDaoImp(){}
	
	public UserDaoImp(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}
	
	// 定义数据库驱动
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	// 定义数据地址
	static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/db-evoluation?useUnicode=true&characterEncoding=UTF-8";

		
	// 定义登录名称
	String username = "root";
	String password = "root";

	public int insertUser(User user) {
		String sql = "insert into user(nickName, account, password, updateTime, createTime) values(?,?,?,?,?)";
		return jdbcTemplate.update(sql, 
				user.getNickName(),
				user.getAccount(), 
				user.getPassword(), 
				user.getUpdateTime(), 
				user.getCreateTime());
	}

	public int updateUser(User user) {
		String sql = "update user set nickName =?, account =?, password =?, updateTime =?, createTime =? where id =?";
		return jdbcTemplate.update(sql, 
				user.getNickName(),
				user.getAccount(), 
				user.getPassword(), 
				user.getUpdateTime(), 
				user.getCreateTime(),
				user.getId());
	}

	public User selectUserById(int id) {
		String sql = "select * from user where id =?";
		return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), id);
	}

	public List<User> selectUserList() {
		String sql = "select * from user";
		return jdbcTemplate.query(sql, new BeanPropertyRowMapper<User>(User.class));
	}

}
