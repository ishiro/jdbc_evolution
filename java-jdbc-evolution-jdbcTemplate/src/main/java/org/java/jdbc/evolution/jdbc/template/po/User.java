package org.java.jdbc.evolution.jdbc.template.po;

import java.util.Date;

public class User {
	//用户ID
	private int id;
	//昵称
	private String nickName;
	//账号
	private String account;
	//密码
	private String password;
	//更新时间
	private Date updateTime;
	//创建时间
	private Date createTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nickName=" + nickName + ", account=" + account + ", password=" + password
				+ ", updateTime=" + updateTime + ", createTime=" + createTime + "]";
	}

	
}
