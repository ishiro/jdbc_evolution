#### 什么是JDBC  

JDBC（Java DataBase Connectivity,java数据库连接）是一种用于执行SQL语句的Java API，可以为多种关系数据库提供统一访问，  
它由一组用Java语言编写的类和接口组成。JDBC提供了一种基准，据此可以构建更高级的工具和接口，使数据库开发人员能够编写数据库应用程序，同时，JDBC也是个商标名。来源：[百度百科](https://baike.baidu.com/item/jdbc/485214?fr=aladdin)  

现在很多使用jdbc的操作就是直接使用已存在的第三方框架，但是这样就会导致我们很多时候选择上比较迷茫。
导致这个问题的原因我个人觉得，是由于我们大家都对于JDBC的过往知之甚少。
所以我就想能不能，把我知道的那些JDBC的知识，
做一个对JDBC演变的收集。

让自己更了解为啥会在这么长的时间里，不断的衍生出各种各样的JDBC框架。

    
##### jdbc数据库操作实例  

如下：  

1、[采用原生 JDBC 操作数据库  ](https://gitee.com/ishiro/jdbc_evolution/wikis/%E9%87%87%E7%94%A8%E5%8E%9F%E7%94%9F-JDBC-%E6%93%8D%E4%BD%9C%E6%95%B0%E6%8D%AE%E5%BA%93--)

2、[采用Spring-jdbcTemplate的方式操作数据库](https://gitee.com/ishiro/jdbc_evolution/wikis/%E9%87%87%E7%94%A8Spring-jdbcTemplate%E7%9A%84%E6%96%B9%E5%BC%8F%E6%93%8D%E4%BD%9C%E6%95%B0%E6%8D%AE%E5%BA%93--)  

3、[采用hibernate操作数据库](https://gitee.com/ishiro/jdbc_evolution/wikis/%E9%87%87%E7%94%A8hibernate%E6%93%8D%E4%BD%9C%E6%95%B0%E6%8D%AE%E5%BA%93)

4、[采用mybatis操作数据库](https://gitee.com/ishiro/jdbc_evolution/wikis/%E9%87%87%E7%94%A8mybatis%E6%93%8D%E4%BD%9C%E6%95%B0%E6%8D%AE%E5%BA%93)

5、[其他]()
<hr>

##### 数据库连接池操作实例：  

1、jdbcTemplate  

2、c3p0  

3、durid  

4、更多

<hr>































