package org.java.jdbc.evolution.jdbc.dao;

import java.util.List;

import org.java.jdbc.evolution.jdbc.po.User;

/**
 * 用户表 db接口
 */
public interface UserDao {
	
	public int insertUser(User user);
	
	public int updateUser(User user);
	
	public User selectUserById(int id);
	
	public List<User> selectUserList();
}
